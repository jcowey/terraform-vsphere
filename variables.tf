variable "datacenter" {
  type = "string"
}

variable "esxi_hosts" {
  type = "list"
}

variable "resource_pool" {
  type = "string"
}

variable "datastore_name" {
  type = "string"
}

variable "network_name" {
  type = "string"
}

variable "template_name" {
  type = "string"
}

variable "virtual_machine_name_prefix" {
  type = "string"
}

variable "virtual_machine_domain" {
  type = "string"
}

variable "virtual_machine_network_address" {
  type = "string"
}

variable "virtual_machine_ip_address_start" {
  default = "200"
}

variable "virtual_machine_gateway" {
  type = "string"
}

variable "virtual_machine_dns_servers" {
  type = "list"
}

variable "management_ssh_keys" {
  type = "list"
}
