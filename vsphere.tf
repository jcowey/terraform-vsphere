resource "vsphere_virtual_machine" "virtual_machines" {
  count            = 7
  name             = "${var.virtual_machine_name_prefix}${count.index}"
  resource_pool_id = "${data.vsphere_resource_pool.resource_pool.id}"
  datastore_id     = "${data.vsphere_datastore.datastore.id}"
  wait_for_guest_net_timeout = -1
  wait_for_guest_net_routable = false
  num_cpus = 4
  memory   = 4096
  guest_id = "${data.vsphere_virtual_machine.template.guest_id}"

  network_interface {
    network_id   = "${data.vsphere_network.network.id}"
    adapter_type = "${data.vsphere_virtual_machine.template.network_interface_types[0]}"
  }

  disk {
    label = "disk0"
    size  = "32"
    thin_provisioned = false
  }

  clone {
    template_uuid = "${data.vsphere_virtual_machine.template.id}"
     customize {
       linux_options {
         host_name = "${var.virtual_machine_name_prefix}${count.index}"
         domain = "${var.virtual_machine_domain}"
      }
      network_interface {
        ipv4_address = "${cidrhost(var.virtual_machine_network_address, var.virtual_machine_ip_address_start + count.index)}"
        ipv4_netmask = "${element(split("/", var.virtual_machine_network_address), 1)}"
      }
      ipv4_gateway = "${var.virtual_machine_gateway}"
      dns_server_list = ["192.168.2.1"]
   }
  }

  provisioner "remote-exec" {
    inline = [
       "curl https://get.docker.com | sh && systemctl enable docker && systemctl start docker",
       "systemctl stop firewalld && systemctl disable firewalld",
       "docker plugin install --grant-all-permissions --alias vsphere vmware/vsphere-storage-for-docker:latest",
       "docker plugin install --grant-all-permissions --alias vfile vmware/vfile:latest VFILE_TIMEOUT_IN_SECOND=9"
    ]

    connection {
      type        = "ssh"
      user        = "root"
      private_key = "${file("~/.ssh/home.key")}"
      host        = "${cidrhost(var.virtual_machine_network_address, var.virtual_machine_ip_address_start + count.index)}"
    }
  }
 }

//resource "null_resource" "ansible" {
//  count = 1
//    triggers = {
//        post-deploy = "${var(vsphere_virtual_machine.virtual_machines, count.index)}" 
//    }
//  provisioner "local-exec" {
//    command = "ansible-playbook ~/projects/ansible-swarm-playbook/swarm-facts.yml -i ~/projects/ansible-swarm-playbook/hosts -e ansible_user=root -e swarm_iface=ens192 -e ansible_ssh_extra_args='-o StrictHostKeyChecking=no'"
//  }
//}
