SHELL := bash

deploy:
	terraform apply -auto-approve && ansible-playbook ~/projects/ansible-swarm-playbook/swarm-facts.yml -i ~/projects/ansible-swarm-playbook/hosts -e ansible_user=root -e swarm_iface=ens192
destroy:
	terraform destroy
